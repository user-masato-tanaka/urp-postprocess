# URP-PostProcess
## 概要
Unity2019.3から追加されたUniversal Render Pipelineでポストプロセスを行うための手法を説明するために作られたプロジェクトです。  
TestSceneで動作確認ができます。

## プロジェクトに追加されているRenderer
このプロジェクトでは複数のRendererが存在します。
- ForwardRenderer  
    プロジェクトに最初から入っていたRenderer  
    現在使用していません
- SimpleForwardRenderer  
    すべてのオブジェクトを普通にレンダリングするRenderer  
- NegaAreaRenderer  
    範囲内のオブジェクトの色反転を行うカスタムエフェクトを追加したRenderer  
- PostProcessRenderer  
    画面全体のポストプロセスを行うためのRenderer  

## Renderer Featureを追加するのに参考になる(であろう)サイト
- [UniversalRenderingExamples](https://github.com/Unity-Technologies/UniversalRenderingExamples)
- [【Unity2019 LWRP】ポストエフェクトを自作してゲームから利用する](https://qiita.com/r-ngtm/items/934493e1e2c364f7019e)
